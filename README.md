In this project I first configured my pom.xml file
with the dependencies required for the tasks. After that
I created my config.properties file, ConfigReader class, 
and Driver class. In my Driver class,
I set all the drivers within a switch for the option to
switch the type of browser the test will be done in. Also, 
within that driver class I set up my implicit wait and 
driver.quit() method. With all that setup, I created my
features file with my scenarios and steps definition page 
for said feature file. For every new html I came across,
I went ahead and created a new element page. For every 
new page I create, I would instantiate it in the steps class 
and create a new object under the before tag in a public void 
method, so I am able to use the elements in the step definitions. 
Seeing that there's a dropdown menu in scenario five I
created an Element Utils class. Within that I created a method
that takes a string and the WebElement of the dropdown. In this
case I called the method and put the string "FamilyAlbum" and
the element of the dropdown.
