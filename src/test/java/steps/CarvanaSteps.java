package steps;

import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.cucumber.datatable.DataTable;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import pages.HomePage;
import pages.LoginPage;
import pages.OrderPage;
import pages.ViewOrderPage;
import utilities.Driver;
import utilities.ElementUtils;

public class CarvanaSteps {
    WebDriver driver;
    LoginPage loginPage;
    HomePage homePage;
    OrderPage orderPage;
    ViewOrderPage viewOrderPage;

    @Before
    public void setUp(){
        driver = Driver.getDriver();
        loginPage = new LoginPage(driver);
        homePage = new HomePage(driver);
        orderPage = new OrderPage(driver);
        viewOrderPage = new ViewOrderPage(driver);
    }

    @Given("user is on {string}")
    public void userIsOn(String url) {
        driver.get(url);
    }

    @When("user enters username as {string}")
    public void userEntersUsernameAs(String username) {
        switch (username) {
            case "abcd": loginPage.username.sendKeys(username); break;
            case "Tester": loginPage.username.sendKeys(username); break;
            default:
                throw new IllegalStateException("Unexpected value: " + username);
        }
    }

    @And("user enters password as {string}")
    public void userEntersPasswordAs(String password) {
        switch (password) {
            case "abcd1234": loginPage.password.sendKeys(password); break;
            case "test": loginPage.password.sendKeys(password); break;
            default:
                throw new IllegalStateException("Unexpected value: " + password);
        }
    }

    @And("user clicks on Login button")
    public void userClicksOnLoginButton() {
        loginPage.enterButton.click();
    }

    @Then("user should see {string} Message")
    public void userShouldSeeMessage(String errorMessage) {
        Assert.assertEquals(errorMessage, loginPage.errorLoginMessage.getText());
    }


    @Then("user should be routed to {string}")
    public void userShouldBeRoutedTo(String url) {
        Assert.assertEquals(url, driver.getCurrentUrl());
    }

    @And("validate {string} are displayed")
    public void validateAreDisplayed(String menuItem) {
        for (int i = 0; i < homePage.menuButtons.size(); i++) {
            homePage.menuButtons.get(i).isDisplayed();
        }
    }

    @When("user clicks on \"Check All\" button")
    public void userClicksOnCheck() {
           homePage.checkAllButton.click();
    }

    @When("user clicks on \"Uncheck All\" button")
    public void userClicksOnUncheck() {
           homePage.uncheckAllButton.click();
    }

    @Then("all rows should be checked")
    public void allRowsShouldBeChecked() {
        for (int i = 0; i < homePage.checkBoxes.size(); i++) {
                homePage.checkBoxes.get(i).isSelected();
        }
    }

    @Then("all rows should be unchecked")
    public void allRowsShouldBeUnchecked() {
        for (int i = 0; i < homePage.checkBoxes.size(); i++) {
            Assert.assertFalse(homePage.checkBoxes.get(i).isSelected());

        }
    }

    @When("user clicks on \"Order\" menu item")
    public void userClicksOnMenuItem() {
        orderPage.orderButton.click();
    }

    @And("user selects \"FamilyAlbum\" as product")
    public void userSelectsAsProduct() {
        ElementUtils.selectOptionFromDropdown("FamilyAlbum", orderPage.productDropDown);
    }

    @And("user enters {int} as quantity")
    public void userEntersAsQuantity(int quantity) {
        orderPage.quantity.sendKeys("quantity");
    }

    @And("user enters all address information")
    public void userEntersAllAddressInformation() {
        orderPage.customerName.sendKeys("Mohammed Musleh");
        orderPage.street.sendKeys("123 Hello St.");
        orderPage.city.sendKeys("Chicago");
        orderPage.state.sendKeys("IL");
        orderPage.zip.sendKeys("12345");
    }

    @And("user enters all payment information")
    public void userEntersAllPaymentInformation() {
        orderPage.cardVisa.click();
        orderPage.cardNumber.sendKeys("1234123412341234");
        orderPage.cardDate.sendKeys("12/25");
    }

    @And("user clicks on \"Process\" button")
    public void userClicksOnButton() {
        orderPage.processButton.click();
    }

    @And("user clicks on \"View all orders\" menu item")
    public void userClicksOnViewAllOrders() {
        orderPage.viewAllOrders.click();
    }

    @Then("user should see their order displayed in the {string} table")
    public void userShouldSeeTheirOrderDisplayedInTheTable(String arg0) {
        viewOrderPage.tableVerify.isDisplayed();
    }

    @And("validate all information entered displayed correct with the order")
    public void validateAllInformationEnteredDisplayedCorrectWithTheOrder(DataTable expectedData) {
        for (int i = 1; i < viewOrderPage.tableInfoVerify.size() - 1; i++) {
            Assert.assertEquals(expectedData.asList().get(i),
                    viewOrderPage.tableInfoVerify.get(i).getText());
        }
    }


    @When("user clicks on “Check All” button")
    public void userClicksOnCheckAllButton() {
        homePage.checkAllButton.click();
    }

    @And("user clicks on “Delete Selected” button")
    public void userClicksOnDeleteSelectedButton() {
        viewOrderPage.deleteAllButton.click();
    }

    @Then("validate all orders are deleted from the “List of All Orders”")
    public void validateAllOrdersAreDeletedFromTheListOfAllOrders() {
        Assert.assertTrue(viewOrderPage.tableInfoVerify.isEmpty());
    }

    @And("validate user sees {string} Message")
    public void validateUserSeesMessage(String message) {
        Assert.assertEquals(viewOrderPage.deleteMessage.getText(), message);
    }
}
