package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

public class ViewOrderPage {
    public ViewOrderPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

    @FindBy(id = "ctl00_MainContent_orderGrid")
    public WebElement tableVerify;

    @FindBy(xpath = "(//table[@class='SampleTable']//tr)[2]")
    public List<WebElement> tableInfoVerify;

    @FindBy(id = "ctl00_MainContent_btnDelete")
    public WebElement deleteAllButton;

    @FindBy(id = "ctl00_MainContent_orderMessage")
    public WebElement deleteMessage;

}
