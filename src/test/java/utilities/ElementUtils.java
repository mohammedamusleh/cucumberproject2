package utilities;

import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.Select;

public class ElementUtils {
    public static void selectOptionFromDropdown(String optionVisibleText, WebElement dropdown){

        Select select = new Select(dropdown);
        select.selectByVisibleText(optionVisibleText);
    }
}
